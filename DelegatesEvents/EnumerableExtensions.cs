﻿
namespace DelegatesEvents;

public static class EnumerableExtensions
{
    // решил сделать метод более универсальным, чем требовалось по заданию
    public static TElement GetMax<TElement,TKey>(this IEnumerable<TElement> sourceCollection,
                                                 Func<TElement, TKey> keySelector) 
        where TKey : IComparable<TKey>
    {
        ArgumentNullException.ThrowIfNull(sourceCollection, nameof(sourceCollection));
        ArgumentNullException.ThrowIfNull(keySelector, nameof(keySelector));
        if (!sourceCollection.Any())
        {
            throw new ArgumentException("Collection can not be empty.");
        }

        TElement max = sourceCollection.First();
        TKey maxKey = keySelector(max);
        foreach (TElement elem in sourceCollection)
        {
            TKey currentKey = keySelector(elem);
            if (currentKey.CompareTo(maxKey) > 0)
            {
                max = elem;
                maxKey = currentKey;
            }
        }
        return max;
    }
}
