﻿
namespace DelegatesEvents;

internal class FileEventArgs(string fileName) : EventArgs
{
    public string FileName { get; private set; } = fileName;
}
