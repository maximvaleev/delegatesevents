﻿
namespace DelegatesEvents;

internal class FileFinder
{
    public event EventHandler<FileEventArgs>? FileFound;
    private bool _isSearchCancelled = false;

    protected virtual void OnFileFound(FileEventArgs e) => FileFound?.Invoke(this, e);

    public void CancelSearch() => _isSearchCancelled = true;

    public void StartSearch(string folderPath)
    {
        if (!Directory.Exists(folderPath)) 
        {
            throw new ArgumentException("Wrong folder path.");
        }
        try
        {
            string[] fileEntries = Directory.GetFiles(folderPath);
            for (int i = 0; i < fileEntries.Length && !_isSearchCancelled; i++)
            {
                OnFileFound(new FileEventArgs(fileEntries[i]));
            }
            Console.WriteLine("Search completed.");
        }
        catch (Exception exc)
        {
            Console.WriteLine("File search failed: " + exc.Message);
        }
    }
}
