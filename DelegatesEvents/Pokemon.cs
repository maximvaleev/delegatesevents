﻿
namespace DelegatesEvents;

internal class Pokemon(string name, float power)
{
    public string Name { get; private set; } = name;
    public float Power { get; private set; } = power;

    public override string ToString() => Name + ": " + Power;
}
