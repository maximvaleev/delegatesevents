﻿/*** Делегаты и события ***

1   Написать обобщённую функцию расширения, находящую и возвращающую максимальный элемент коллекции.
    Функция должна принимать на вход делегат, преобразующий входной тип в число для возможности поиска 
    максимального значения.
    public static T GetMax(this IEnumerable collection, Func<T, float> convertToNumber) where T : class;
2   Написать класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла;
3   Оформить событие и его аргументы с использованием .NET соглашений:
    public event EventHandler FileFound;
    FileArgs – будет содержать имя файла и наследоваться от EventArgs
4   Добавить возможность отмены дальнейшего поиска из обработчика;
5   Вывести в консоль сообщения, возникающие при срабатывании событий и результат поиска максимального 
    элемента. */

namespace DelegatesEvents;

internal class Program
{
    static void Main()
    {
        TestGetMax();
        TestFileFinderEvents(@"C:\Windows");
    }

    private static void TestGetMax()
    {
        List<Pokemon> pokemons =
        [
            new Pokemon("Pikachu", 9_000f),
            new Pokemon("Snorlax", 12_000f),
            new Pokemon("Cinderace", 120.5f),
            new Pokemon("Lucario", 500f),
            new Pokemon("Wigglytuff", 210.7f),
        ];
        Console.WriteLine("The most powerfull pokemon is...");
        Console.WriteLine(pokemons.GetMax(x => x.Power));
        Console.WriteLine();
    }

    private static void TestFileFinderEvents(string folderPath)
    {
        FileFinder finder = new();
        FileFinderSubscriber sub1 = new(finder, "Subscriber #1");
        FileFinderSubscriber sub2 = new(finder, "Subscriber #2");
        finder.StartSearch(folderPath);
        sub1.Dispose();
        sub2.Dispose();
    }
}
