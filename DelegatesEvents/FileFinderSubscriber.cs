﻿
namespace DelegatesEvents;

internal class FileFinderSubscriber : IDisposable
{
    public string Name { get; private set; }
    private readonly FileFinder _publisher;
    private readonly Random _random;
    private const int CancellationProbabilityPercent = 5;

    public FileFinderSubscriber(FileFinder publisher, string name = nameof(FileFinderSubscriber))
    {
        Name = name;
        _random = new Random();
        _publisher = publisher;
        _publisher.FileFound += HandleFileFoundEvent;
    }

    private void HandleFileFoundEvent(object? sender, FileEventArgs e)
    {
        Console.WriteLine($"Subscriber '{Name}' received event with file '{e.FileName}'.");
        if (_random.Next(100) < CancellationProbabilityPercent)
        {
            Console.WriteLine($"--- Subscriber '{Name}' decides to cancel the search.");
            _publisher.CancelSearch();
        }
    }

    public void Dispose() => _publisher.FileFound -= HandleFileFoundEvent;

}
